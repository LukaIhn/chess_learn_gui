import datetime
import os
from abc import abstractmethod, ABC
from typing import List
import time

import numpy as np
import pandas as pd

from pandas import HDFStore
from class_registry import ClassRegistry

from config import METRIC_NAMES, LOG_DIR

metrics_registry = ClassRegistry('name')


class StatisticsHandler:

    def __init__(self, log_dir: str = LOG_DIR, metric_names: List[str] = METRIC_NAMES):
        self.memorization_start = None
        self.reconstructing_start = None
        self.error_analysis_start = None

        self.log_dir = log_dir
        self.metrics = {m: metrics_registry.get(m) for m in metric_names}

        self.log_file_name = datetime.datetime.now().strftime('%Y-%m-%d')

        self.last_record = dict()

    def get_message(self):
        new_dict = {}
        for k, v in self.last_record.items():
            if k != 'time':
                if type(v) == float:
                    v = '%.2f' % v
                new_dict[k] = v

        return str(new_dict).replace(',', '\n')

    def path_to_logs(self) -> str:
        return os.path.join(self.log_dir, self.log_file_name)

    def append_new_record(self, target: np.ndarray, reconstructed: np.ndarray, id_: int):
        now = time.time()

        result = {}
        for name, metric in self.metrics.items():
            result[name] = [metric.calculate(target, reconstructed)]

        result['time'] = now
        result['id_'] = id_

        result['time_to_memorize'] = self.reconstructing_start - self.memorization_start
        result['time_to_reproduce'] = self.error_analysis_start - self.reconstructing_start

        with HDFStore(self.path_to_logs(), mode='a') as store:
            store.append('data', pd.DataFrame(result))

        self.last_record = result

    def measure_time(self, which_time: str):
        if which_time == 'memorization':
            self.memorization_start = time.time()
        elif which_time == 'reconstructing':
            self.reconstructing_start = time.time()
        elif which_time == 'error_analysis':
            self.error_analysis_start = time.time()


class MetricAbstract(ABC):
    name = None
    last_value = None

    @abstractmethod
    def calculate(self, target: np.ndarray, reconstructed: np.ndarray) -> float:
        pass


@metrics_registry.register
class IOU(MetricAbstract):
    name = 'IoU'

    def calculate(self, target: np.ndarray, reconstructed: np.ndarray) -> float:
        intersection = ((target != 0) & (reconstructed != 0) & (target == reconstructed)).sum()
        union = ((target != 0) | (reconstructed != 0)).sum()
        IoU = intersection / union
        return IoU


@metrics_registry.register
class NumberOfFigures(MetricAbstract):
    name = 'number_of_figures'

    def calculate(self, target: np.ndarray, reconstructed: np.ndarray) -> float:
        return (target != 0).sum()

import tkinter as tk  # help to creat gui
import numpy as np

from PIL import Image, ImageTk
from PIL.ImageTk import PhotoImage

import colors as c
from game_handler import GameHandler, DeskLoader
from utils import get_random_figure

GRID_SIZE = 8
BOARD_WIDTH = 430
SQUARE_SIZE = BOARD_WIDTH // GRID_SIZE


class ChessSquare(tk.Button):
    def __init__(self, *args, **kwargs):
        super(ChessSquare, self).__init__(*args, **kwargs)
        self.empty()

    def put_figure(self, figure):
        im = Image.open(c.images[figure])
        ph = ImageTk.PhotoImage(im)
        self.configure(image=ph)
        self.image = ph

        self.current_figure = figure

    def empty(self):
        self.put_figure(c.BLANK)


class Hand:
    def __init__(self, utility_square: ChessSquare, empty_square: ChessSquare):
        self.current_figure = c.BLANK

        self.utility_square = utility_square
        self.utility_square.put_figure(c.BLANK)

        self.empty_square = empty_square

        empty_square.configure(command=self.drop_figure)

    def board_square_clicked(self, square: ChessSquare, row, col):
        """
        Used for handling interactions with board squares. E.g. move a figure, take from board, put on board .
        :param square:
        :param row:
        :param col:
        :return:
        """
        if not self.carries_figure():
            self.set_figure(square.current_figure)
            square.empty()
        else:
            figure_from_hand = self.current_figure
            self.set_figure(square.current_figure)
            square.put_figure(figure_from_hand)

    def set_figure(self, figure):
        self.current_figure = figure
        self.utility_square.put_figure(figure)

    def collection_square_clicked(self, square, row, col):
        self.set_figure(square.current_figure)

    def carries_figure(self):
        return self.current_figure != c.BLANK

    def drop_figure(self):
        self.set_figure(c.BLANK)

    def side_square_clicked(self, square, i, j):
        """
        Used for handling interactions with helper squares. E.g. move, take, put.
        """
        raise NotImplementedError()


class ChessBoard(tk.Frame):
    def __init__(self, hand, *args, **kwargs):
        super(ChessBoard, self).__init__(*args, **kwargs)

        self.hand = hand

        self.board_squares = dict()

        for i in range(GRID_SIZE):
            for j in range(GRID_SIZE):
                color = c.WHITE if (j + i) % 2 == 0 else c.BLACK

                chess_square = None
                chess_square = ChessSquare(
                    self,
                    bg=color,
                    activebackground=color,
                    width=BOARD_WIDTH // GRID_SIZE,
                    height=BOARD_WIDTH // GRID_SIZE,
                )

                def square_clicked(i=i, j=j, chess_square=chess_square):
                    return self.hand.board_square_clicked(chess_square, i, j)

                chess_square.configure(command=square_clicked)
                chess_square.grid(row=i, column=j)

                self.board_squares[i, j] = chess_square

    def update_board(self, board_data: np.ndarray):
        for (i, j), square in self.board_squares.items():
            figure_id = int(board_data[i, j, ...])
            square.put_figure(figure_id)

    def as_np_array(self):
        board_data = np.zeros((8, 8), dtype='uint8')
        for (i, j), square in self.board_squares.items():
            board_data[i, j] = square.current_figure
        return board_data

    def show_empty_desk(self):
        empty_board_data = np.zeros((8, 8), dtype='uint8')
        self.update_board(empty_board_data)


class Game():
    def __init__(self, handler):
        self.root = tk.Tk()
        # self.root.geometry("1300x1300")

        self.main_frame = tk.Frame(self.root, width=BOARD_WIDTH + 10*SQUARE_SIZE)
        self.main_frame.grid()


        hand_chess_square = ChessSquare(
            self.main_frame,
            bg=c.WHITE,
            activebackground=c.WHITE,
            width=SQUARE_SIZE,
            height=SQUARE_SIZE,
        )
        empty_square = ChessSquare(
            self.main_frame,
            fg=c.WHITE,
            bg=c.GREY,
            activebackground=c.GREY,
            width=SQUARE_SIZE,
            height=SQUARE_SIZE,
        )
        empty_square.config(text="EMPTY")
        hand_chess_square.place(x=0, y=0)
        empty_square.place(x=0, y=SQUARE_SIZE)

        self.hand = Hand(hand_chess_square, empty_square)
        self.root.title("Reconstruct chess board")

        self.collection_grid = tk.Frame(
            self.main_frame, bg=c.GRID_COLOR, width=6 * SQUARE_SIZE, height=2 * SQUARE_SIZE
        )
        self.collection_grid.grid()

        self.chess_board = ChessBoard(master=self.main_frame, hand=self.hand,
                                      bg=c.GRID_COLOR, width=BOARD_WIDTH, height=BOARD_WIDTH)
        self.chess_board.grid(pady=(160, 50), padx=(20,50))

        self.make_collection_board()


        self.game_button_text = tk.StringVar(value='CLICK ME')
        self.game_button = tk.Button(self.main_frame, textvariable=self.game_button_text, bg=c.WHITE, font=("arial", 20, "bold"), activebackground='grey')
        self.game_button.configure(height=2, width=35)
        self.game_button.place(x=0, y=15 + 2*SQUARE_SIZE)
        self.game_button.vartext = self.game_button_text

        self.switch_view_button_text = tk.StringVar(value='<->')
        self.switch_view_button = tk.Button(self.main_frame, textvariable=self.switch_view_button_text, bg=c.WHITE, font=("arial", 15, "bold"), activebackground='grey')
        self.switch_view_button.place(x=79 + 7*SQUARE_SIZE, y=0, height=120, width=2* SQUARE_SIZE)
        self.switch_view_button.vartext = self.switch_view_button_text

        self.info_label_text = tk.StringVar(value='____!')
        self.game_info_label = tk.Label(self.main_frame, textvariable=self.info_label_text, bg='white', font=("arial", 13, "bold"))
        self.game_info_label.configure(height=5, width=55)
        self.game_info_label.vartext = self.info_label_text
        self.game_info_label.place(x=0, y=15 + 3*SQUARE_SIZE)
        self.handler = handler
        self.handler.bind_to_ui(self.game_button, self.chess_board, self.game_info_label, self.switch_view_button)

        self.skip_game_button_text = tk.StringVar(value='EXIT_CURRENT_GAME')
        self.skip_game_button = tk.Button(self.main_frame, textvariable=self.skip_game_button_text, bg=c.WHITE, font=("arial", 15, "bold"), activebackground='grey')
        self.skip_game_button.place(x=SQUARE_SIZE, y=10+ 14*SQUARE_SIZE, height=50, width=BOARD_WIDTH)
        self.skip_game_button.vartext = self.skip_game_button_text
        self.skip_game_button.configure(command=self.handler.reset)

        self.root.mainloop()

    def make_collection_board(self):
        self.collection_squares = []

        for i in range(2):
            color = c.WHITE if i == 0 else c.BLACK
            row = []

            for j in range(6):
                chess_square = ChessSquare(
                    self.collection_grid,
                    bg=color,
                    activebackground=color,
                    width=BOARD_WIDTH // GRID_SIZE,
                    height=BOARD_WIDTH // GRID_SIZE,
                )

                def square_clicked(i=i, j=j, chess_square=chess_square):
                    return self.hand.collection_square_clicked(chess_square, i, j)

                chess_square.configure(command=square_clicked)
                chess_square.grid(row=i, column=j)
                chess_square.put_figure(c.ALL_FIGURES[i * 6 + j + 1])

                row.append((chess_square))

            self.collection_squares.append(row)

        # score_frame = tk.Frame(self)
        # score_frame.place(relx=0.5, y=45, anchor='center')
        # tk.Label(
        #     score_frame,
        #     text="Score",
        #     font=c.DEFAULT_FONT,
        # ).grid(row=0)
        # self.score_label = tk.Label(score_frame, text='0', font=c.DEFAULT_FONT)
        # self.score_label.grid(row=1)


desk_loader = DeskLoader()
h = GameHandler(desk_loader)
Game(h)

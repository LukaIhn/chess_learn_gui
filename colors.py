import os
import tkinter as tk  # help to creat gui

from PIL import ImageTk, Image

GRID_COLOR = 'grey'

WHITE = 'white'
GREY = 'grey'
BLACK = 'brown'

DEFAULT_FONT = ('Helvetica', 36, 'bold')

# piece names
ALL_FIGURES = [BLANK, PAWNB, KNIGHTB, BISHOPB, ROOKB, QUEENB, KINGB, PAWNW, KNIGHTW, BISHOPW, ROOKW, QUEENW,
               KINGW] = list(range(13))

IMAGE_PATH = './Images/60/'
# Images/60
blank = os.path.join(IMAGE_PATH, 'blank.png')
bishopB = os.path.join(IMAGE_PATH, 'bB.png')
bishopW = os.path.join(IMAGE_PATH, 'wB.png')
pawnB = os.path.join(IMAGE_PATH, 'bP.png')
pawnW = os.path.join(IMAGE_PATH, 'wP.png')
knightB = os.path.join(IMAGE_PATH, 'bN.png')
knightW = os.path.join(IMAGE_PATH, 'wN.png')
rookB = os.path.join(IMAGE_PATH, 'bR.png')
rookW = os.path.join(IMAGE_PATH, 'wR.png')
queenB = os.path.join(IMAGE_PATH, 'bQ.png')
queenW = os.path.join(IMAGE_PATH, 'wQ.png')
kingB = os.path.join(IMAGE_PATH, 'bK.png')
kingW = os.path.join(IMAGE_PATH, 'wK.png')

images = {BISHOPB: bishopB, BISHOPW: bishopW, PAWNB: pawnB, PAWNW: pawnW,
          KNIGHTB: knightB, KNIGHTW: knightW,
          ROOKB: rookB, ROOKW: rookW, KINGB: kingB, KINGW: kingW,
          QUEENB: queenB, QUEENW: queenW, BLANK: blank}
#
# images = {}
# for k, v in image_pathes.items():
#     # im = Image.open(v)
#     ph = tk.PhotoImage(file=v)
#     images[k] = ph

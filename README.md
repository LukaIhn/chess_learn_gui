1. Create virtual environment using `conda` or `venv` for `python 3.7`;
2. Activate that virtual environment. (for conda `source activate $VENVNAME`, for venv `source $PATH_TO_VENV/$VENVNAME/bin/activate`)
3. Install the requirements using `pip install -r $PROJECTDIR/requirements.txt`;
4. Download data from `www.kaggle.com/llkihn/chess-positions` to `$PROJECTDIR/data/data_sets/`;
5. Run `gui_main.py` using python from the virtual environment to play the game.

import os

METRIC_NAMES = ['IoU', 'number_of_figures']
LOG_DIR = os.path.join('.', 'data', 'game_logs')
DATA_DIR = os.path.join('.', 'data', 'data_sets')

DESK_FILES = [
    'ficsgamesdb_2017_standard2000_nomovetimes_162378.h5',
    'ficsgamesdb_2018_standard2000_nomovetimes_162106.h5',
    'ficsgamesdb_2019_standard2000_nomovetimes_162105.h5',
]
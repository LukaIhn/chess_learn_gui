import os
import random

import h5py
import numpy as np
import tkinter as tk

import typing

from config import DESK_FILES, DATA_DIR
from metrics import StatisticsHandler


def read_data(file_name: str, data_dir: str = DATA_DIR) -> np.ndarray:
    file_path = os.path.join(data_dir, file_name)
    with h5py.File(file_path, 'r') as data_store:
        data = data_store['input_position'].value

    return data


x_train, x_test = read_data(DESK_FILES[0]), read_data(DESK_FILES[1])

USE_TOP = 1000
x_train = x_train.astype('uint8')[-USE_TOP:, ...]
x_test = x_test.astype('uint8')[-USE_TOP:, ...]
print(x_train.shape)
print(x_test.shape)


class DeskLoader:
    def __init__(self, train: np.ndarray = x_train, test: np.ndarray = x_test):
        self.train = train
        self.test = test

    def get_pattern_by_id(self, id_: int, train: bool = True):
        if train:
            return self.train[id_]
        else:
            return self.test[id_]

    def iterate_over_train(self) -> typing.Generator:
        ids = list(range(len(self.train)))
        random.shuffle(ids)

        for i in ids:
            p = self.get_pattern_by_id(i)
            if self.condition_satisfied(p):
                yield i, p

    def condition_satisfied(self, p) -> bool:
        return True


class GameStatus:
    ON_HOLD = 0,
    MEMORIZING = 1,
    RECONSTRUCTING = 2,
    AFTER_GAME = 3


class GameHandler:
    game_button: tk.Button = None
    board_ui: 'ChessBoard' = None
    info_label_ui = None

    def __init__(self, data: DeskLoader):
        self.current_status = GameStatus.ON_HOLD
        self.data = data

        self.current_target_board: np.ndarray = self.get_empty_board()
        self.current_predicted_board = self.get_empty_board()

        self.game_iterator = self.data.iterate_over_train()
        self.statistic_handler = StatisticsHandler()

        self.current_board_is_target = False

    def bind_to_ui(self, game_button: tk.Button, board_ui: 'ChessBoard', info_label_ui: tk.Label,
                   switch_view_button: tk.Button):
        self.button = game_button
        self.board_ui = board_ui
        self.info_label_ui = info_label_ui
        game_button.configure(command=lambda b=game_button: self.game_button_clicked(button=b))
        self.game_button = game_button
        self.switch_button = switch_view_button
        self.switch_button.configure(command=self.switch_button_clicked)

    def game_button_clicked(self, button: tk.Button):
        if self.current_status == GameStatus.ON_HOLD:
            self.current_status = GameStatus.MEMORIZING
            self.on_memorizing()
        elif self.current_status == GameStatus.MEMORIZING:
            self.current_status = GameStatus.RECONSTRUCTING
            self.on_reconstruction()
        elif self.current_status == GameStatus.RECONSTRUCTING:
            self.current_status = GameStatus.AFTER_GAME
            self.on_results()
        elif self.current_status == GameStatus.AFTER_GAME:
            self.current_status = GameStatus.ON_HOLD
            self.on_hold()
        self.game_button.master.master.update()

    def on_memorizing(self):
        print('memorization:')
        self.current_target_board_id, p = next(self.game_iterator)
        self.current_target_board = p
        self.current_predicted_board = self.get_empty_board()
        self._switch_to_board(to_target_board=True)

        self.statistic_handler.measure_time('memorization')

        self.game_button.vartext.set('Start reconstructing!')
        self.info_label_ui.vartext.set('Try to memorize and click `reconstruct`\n when ready.')

    def on_reconstruction(self):
        print('trying to reconstruct:')
        self._switch_to_board(to_target_board=False)
        self.statistic_handler.measure_time('reconstructing')

        self.game_button.vartext.set('Check my solution!')
        self.info_label_ui.vartext.set('When finished placing figures \n click `Check my solution`.')

    def on_results(self):
        print('game results:')
        self.statistic_handler.measure_time('error_analysis')

        self._switch_to_board(to_target_board=True)
        self.statistic_handler.append_new_record(self.current_target_board, self.current_predicted_board,
                                                 self.current_target_board_id)

        message = self.statistic_handler.get_message()
        self.info_label_ui.vartext.set(message)
        self.game_button.vartext.set('Finish game')

    def on_hold(self):
        print('Want to start new game?:')
        self.board_ui.show_empty_desk()
        self.info_label_ui.vartext.set('')
        self.game_button.vartext.set('Start new game!')

    def reset(self):
        self.current_status = GameStatus.ON_HOLD
        self.on_hold()

    def get_empty_board(self) -> np.ndarray:
        return np.zeros((8, 8), dtype='uint8')

    def switch_button_clicked(self):
        opposite_to_current = not self.current_board_is_target
        self._switch_to_board(opposite_to_current)

    def _switch_to_board(self, to_target_board: bool = True):
        # so that the progress is not lost
        if not self.current_board_is_target:
            self.current_predicted_board = self.board_ui.as_np_array()

        if to_target_board:
            self.board_ui.update_board(self.current_target_board)
            self.switch_button.vartext.set('Targ')
        else:
            self.board_ui.update_board(self.current_predicted_board)
            self.switch_button.vartext.set('Pred')

        self.current_board_is_target = to_target_board

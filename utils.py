import random
import chess
import numpy as np

from colors import ALL_FIGURES


def get_random_figure() -> int:
    return random.randint(0, ALL_FIGURES[-1])


def n_to_col_and_row(i):
    row = i // 8
    col = i % 8
    return col, row


def row_col_to_n(col, row):
    return row * 8 + col


def id_to_figure(figure_id):
    color = (figure_id - 1) // 6
    figure = 1 + (figure_id - 1) % 6
    return color, figure

def matrix_to_board(desk: np.ndarray) -> chess.Board:
    '''same as position_list_one_hot except this is converts pieces to matrices with
    numbers between 0 and 12'''
    board = chess.Board(fen=None)

    for i, square in enumerate(chess.SQUARES_180):
        col, row = n_to_col_and_row(i)
        figure_id = desk[row, col]

        color, figure = id_to_figure(figure_id)

        #         0
        #         1 2 3  4  5  6
        #         7 8 9 10 11 12
        if figure_id:
            board._set_piece_at(square, figure, color)
    #         print(figure, color)

    return board
